#ifndef VNATRACE_H
#define VNATRACE_H

#include <qreadwritelock.h>
#include <QMap>

#include <Units.h>
#include <AnalyzerItem.h>
#include <IndexRange.h>

#include "TraceResult.h"
#include "Properties/EnumProperty.h"
#include "Properties/IntProperty.h"
#include "Properties/BoolProperty.h"
#include "Properties/UnitProperty.h"
#include "VnaMarkerCollection.h"

#include "TraceStages/FormatTraceStage.h"
#include "TraceStages/MeasurementSelectTraceStage.h"
#include "TraceStages/SmoothingTraceStage.h"

namespace Planar {
namespace VNA {
namespace Analyzer {

class VnaChannel;

class ANALYZERSHARED_EXPORT VnaTrace : public AnalyzerItem
{
    Q_OBJECT
    ANALYZER_ITEM_UUID(PLANAR_UUID)

    Q_PROPERTY(MeasurementSelectTraceStage* measurementSelectStage READ measurementSelectStage NOTIFY
               tracePropertyAdded)
    Q_PROPERTY(FormatTraceStage* formatStage READ formatStage NOTIFY tracePropertyAdded)
    Q_PROPERTY(SmoothingTraceStage* smoothingStage READ smoothingStage NOTIFY tracePropertyAdded)

    Q_PROPERTY(EnumPropertyBase* parameter READ parameter NOTIFY tracePropertyAdded)

    Q_PROPERTY(FactoryPropertyBase* format READ format NOTIFY tracePropertyAdded)
    Q_PROPERTY(UnitProperty* scaleReference READ scaleReference NOTIFY tracePropertyAdded)
    Q_PROPERTY(UnitProperty* scaleDivision READ scaleDivision NOTIFY tracePropertyAdded)
    Q_PROPERTY(IntProperty* scalePosition READ scalePosition NOTIFY tracePropertyAdded)
    //временное свойство для оценки правильности работы алгоритмов с IPP
    Q_PROPERTY(BoolProperty* isIppOptimized READ isIppOptimized NOTIFY tracePropertyAdded)

    Q_PROPERTY(BoolProperty* isAutoScale READ isAutoScale NOTIFY tracePropertyAdded)

    Q_PROPERTY(Planar::Unit valueUnit READ valueUnit NOTIFY pipelineRebuilded)
    Q_PROPERTY(Planar::Unit argumentUnit READ argumentUnit NOTIFY pipelineRebuilded)

    Q_PROPERTY(VnaMarkerCollection* markers READ markers NOTIFY tracePropertyAdded)

public:
    VnaTrace(VnaChannel* channel, MeasurementParameter param = MeasurementParameter::S11,
             QUuid format = LogMagFormatUuid);

    ~VnaTrace() override;

    VnaMarkerCollection* markers();

    Q_INVOKABLE VnaMarker* activeMarker();  // Q_INVOKABLE для единообразия))

    TraceResult* result();

    VnaChannel* channel() const;

    void approximateValue(const Unit& argument, Unit& resultRe, Unit& resultIm);
    void approximateValue(const Unit& argument, Unit& result);

    int size() const;
    Unit valueUnit();
    Unit argumentUnit();
    Unit argument(int index);

    // число делений шкалы, независимо от того, чем определяется - каналом или диаграммой
    Q_INVOKABLE int scaleDivisionCount();

    BoolProperty* isAutoScale();
    BoolProperty* isIppOptimized();

    EnumProperty<MeasurementParameter>* parameter() const;
    //TODO: временный геттер: параметр должен браться из контрола (parameter.value(), как раньше, только value() должен возвращать VnaParameter
    VnaParameter selectedParameter() const;

    FactoryProperty<QUuid>* format() const;
    IntProperty* scalePosition();
    UnitProperty* scaleReference();
    UnitProperty* scaleDivision();
    bool isPolar() const;

    void copyOutputTo(double* dstRe, double* dstIm, int start, int stop) const;
    void copyOutputTo(std::complex<double>* dst, int start, int stop) const;
    void copyOutputTo(Planar::Dsp::ComplexVector64& dst, int start, int stop) const;
    void copyOutputTo(Planar::Dsp::RealVector64& dst, int start, int stop) const;

    void copyOutputTo(double* dstRe, double* dstIm) const;
    void copyOutputTo(std::complex<double>* dst) const;
    void copyOutputTo(Planar::Dsp::ComplexVector64& dst) const;
    void copyOutputTo(Planar::Dsp::RealVector64& dst) const;

    Planar::Dsp::AbstractVector* copy(int start, int stop) const;
    Planar::Dsp::ComplexVector64* copyAsComplex(int start, int stop) const;
    Planar::Dsp::RealVector64* copyAsReal(int start, int stop) const;

    Planar::Dsp::AbstractVector* copy() const;
    Planar::Dsp::ComplexVector64* copyAsComplex() const;
    Planar::Dsp::RealVector64* copyAsReal() const;

    MeasurementSelectTraceStage* measurementSelectStage();
    FormatTraceStage* formatStage();
    SmoothingTraceStage* smoothingStage();

public slots:
    void onParameterChanged();
    void onFormatChanged();
    void onChannelConfigurationChanged();

signals:
    void dataChanged(unsigned msecs);
    void activeMarkerChanged(int index);
    void pipelineRebuilded();
    void tracePropertyAdded();

    void parameterChanged();

protected:
    void reconfigureStages();

    Dsp::ComplexVector64* complexOutput() const;
    Dsp::RealVector64* realOutput() const;

private slots:
    void onDataReady(Planar::Dsp::AbstractVector* data, Planar::IndexRange dirtyRange);

private:
    VnaChannel* _channel;

    VnaMarkerCollection _markerCollection;

    // автомасштабирование, свойства _scaleXxx игнорируются
    BoolProperty _isAutoScale;
    BoolProperty _isIppOptimized;

    Planar::Dsp::AbstractVector* _output;
    IndexRange _outputIndexRange;
    mutable QReadWriteLock _outputLock;

    Unit _phaseOffset;
    Unit _electricalDelay;

    static QMap<QString, TraceScaleInfo*> _scaleInfo;

    MeasurementSelectTraceStage _measurementSelectStage;
    FormatTraceStage _formatStage;
    SmoothingTraceStage _smoothingStage;
};

inline VnaMarkerCollection* VnaTrace::markers()
{
    return  &_markerCollection;
}

inline VnaMarker* VnaTrace::activeMarker()
{
    return _markerCollection.activeItem();
}

inline VnaChannel* VnaTrace::channel() const
{
    return _channel;
}

inline BoolProperty* VnaTrace::isAutoScale()
{
    return &_isAutoScale;
}

inline BoolProperty* VnaTrace::isIppOptimized()
{
    return &_isIppOptimized;
}

inline EnumProperty<MeasurementParameter>* VnaTrace::parameter() const
{
    return _measurementSelectStage.parameter();
}

inline FactoryProperty<QUuid>* VnaTrace::format() const
{
    return _formatStage.format();
}

inline IntProperty* VnaTrace::scalePosition()
{
    return _formatStage.scalePosition();
}

inline UnitProperty* VnaTrace::scaleReference()
{
    return _formatStage.scaleReference();
}

inline UnitProperty* VnaTrace::scaleDivision()
{
    return _formatStage.scaleDivision();
}

inline bool VnaTrace::isPolar() const
{
    return _formatStage.isPolar();
}

inline void VnaTrace::copyOutputTo(double* dstRe, double* dstIm) const
{
    copyOutputTo(dstRe, dstIm, 0, size() - 1);
}

inline void VnaTrace::copyOutputTo(std::complex<double>* dst) const
{
    copyOutputTo(dst, 0, size() - 1);
}

inline void VnaTrace::copyOutputTo(Dsp::ComplexVector64& dst) const
{
    copyOutputTo(dst, 0, size() - 1);
}

inline void VnaTrace::copyOutputTo(Dsp::RealVector64& dst) const
{
    copyOutputTo(dst, 0, size() - 1);
}

inline Dsp::AbstractVector* VnaTrace::copy() const
{
    return copy(0, size() - 1);
}

inline Dsp::ComplexVector64* VnaTrace::copyAsComplex() const
{
    return copyAsComplex(0, size() - 1);
}

inline Dsp::RealVector64* VnaTrace::copyAsReal() const
{
    return copyAsReal(0, size() - 1);
}

inline MeasurementSelectTraceStage* VnaTrace::measurementSelectStage()
{
    return &_measurementSelectStage;
}

inline FormatTraceStage* VnaTrace::formatStage()
{
    return &_formatStage;
}

inline SmoothingTraceStage* VnaTrace::smoothingStage()
{
    return &_smoothingStage;
}

inline Dsp::ComplexVector64* VnaTrace::complexOutput() const
{
    return static_cast<Dsp::ComplexVector64*>(_output);
}

inline Dsp::RealVector64* VnaTrace::realOutput() const
{
    return static_cast<Dsp::RealVector64*>(_output);
}

} //namespace Analyzer
} //namespace VNA
} //namespace Planar

Q_DECLARE_METATYPE(Planar::VNA::Analyzer::VnaTrace*)

#endif // VNATRACE_H
