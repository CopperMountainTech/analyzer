#pragma once

#include <AnalyzerItemCollection.h>

#include "VnaMarker.h"

namespace Planar {
namespace VNA {
namespace Analyzer {

class VnaTrace;

class ANALYZERSHARED_EXPORT VnaMarkerCollection : public AnalyzerItemCollection<VnaMarker>
{
    Q_OBJECT
    ANALYZER_ITEM_UUID(PLANAR_UUID)
    Q_PROPERTY(VnaMarker* activeMarker READ activeMarker NOTIFY activeItemChanged)

public:
    VnaMarkerCollection(VnaTrace* parent);

    Q_INVOKABLE VnaMarker* marker(int index) const;

    VnaMarker* activeMarker() const;

    Q_INVOKABLE void requestAddMarker();
    Q_INVOKABLE void requestAddMarker(bool isReference);

    void addChild(QVariantList params) override;

private:
    VnaTrace* owner();
};

} //namespace Analyzer
} //namespace VNA
} //namespace Planar

Q_DECLARE_METATYPE(Planar::VNA::Analyzer::VnaMarkerCollection*)
