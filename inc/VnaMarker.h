#ifndef VNAMARKER_H
#define VNAMARKER_H

#include <complex>

#include <Units.h>
#include <AnalyzerItem.h>
#include <Properties/UnitProperty.h>

#include "AnalyzerCommon.h"

namespace Planar {
namespace VNA {
namespace Analyzer {

class VnaTrace;

class ANALYZERSHARED_EXPORT VnaMarker : public AnalyzerItem
{
    Q_OBJECT
    ANALYZER_ITEM_UUID(PLANAR_UUID)
    Q_PROPERTY(UnitProperty* argument READ argument NOTIFY markerPropertyAdded)
    Q_PROPERTY(UnitProperty* valueX READ valueX NOTIFY markerPropertyAdded)
    Q_PROPERTY(UnitProperty* valueY READ valueY NOTIFY markerPropertyAdded)
public:
    VnaMarker(VnaTrace* trace, bool isReference = false);
    ~VnaMarker() override;
    void updateValue();
    void reset();
    UnitProperty* argument();
    UnitProperty* valueX();
    UnitProperty* valueY();

signals:
    void valueChanged();
    void markerPropertyAdded();

private:
    UnitProperty _argument;
    UnitProperty* _valueX;
    UnitProperty* _valueY;
    //TODO: переделать в Unit. На данный момент нет нормального отображения данных полей.
    VnaTrace* _trace;
    bool _isReference;
};

} //namespace Analyzer
} //namespace VNA
} //namespace Planar

#endif // VNAMARKER_H
