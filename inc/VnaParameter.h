#pragma once

#include <UuidConstexpr.h>

#include "AnalyzerCommon.h"

namespace Planar {
namespace VNA {
namespace Analyzer {

static constexpr QUuid SParameterUuid =
    CreateUuid(0x4ee466ff, 0xfeba, 0x4860, 0xb0cd, 0x9ee647dcf4cd);

static constexpr QUuid ReferenceRawUuid =
    CreateUuid(0x91ab88af, 0x598d, 0x4a6d, 0xa19d, 0x3c2dc790bb97);

static constexpr QUuid TestRawUuid =
    CreateUuid(0xadf7fa24, 0x1979, 0x438d, 0xacdd, 0x326fdb78cc5d);

struct VnaParameter {
    QUuid type;

    // нумерация портов от 0
    int activePort;
    int testPort;
};

} //namespace Analyzer
} //namespace VNA
} //namespace Planar
