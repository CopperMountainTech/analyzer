#ifndef TRACERESULT_H
#define TRACERESULT_H

#include <ComplexVector.h>
#include <IndexRange.h>

#include "AnalyzerCommon.h"
namespace Planar {
namespace VNA {
namespace Analyzer {

class VnaMarker;

class ANALYZERSHARED_EXPORT TraceResult
{
public:
    TraceResult(int size, bool isPolar);
    TraceResult(const Planar::Dsp::RealVector64& vector, bool isPolar);
    ~TraceResult();

    bool isPolar() const;

    double y(double argument) const;
    double x(double argument) const;

    double y(const Unit& argument) const;
    double x(const Unit& argument) const;

    double y(VnaMarker* marker) const;
    double x(VnaMarker* marker) const;

    Unit yUnit(const Unit& argument) const;
    Unit xUnit(const Unit& argument) const;

    Unit yUnit(VnaMarker* marker) const;
    Unit xUnit(VnaMarker* marker) const;

    //TODO: заменить на AbstractVector
    Planar::Dsp::RealVector64* points;
    Planar::Dsp::RealVector64* pointsPolarIm;
    Planar::IndexRange dirtyRange;

private:
    Dsp::Complex64 approximateAsComplex(double argument) const;
    bool _isPolar;
};

} //namespace Analyzer
} //namespace VNA
} //namespace Planar

#endif // TRACERESULT_H
