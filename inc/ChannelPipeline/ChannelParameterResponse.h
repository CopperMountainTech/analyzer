#pragma once

#include <QVector>

#include "ActivePortParameterResponse.h"

namespace Planar {
namespace VNA {
namespace Analyzer {

class ANALYZERSHARED_EXPORT ChannelParameterResponse
{
public:
    ChannelParameterResponse(int activePortCount, int receiverPortCount, int pointCount);
    ~ChannelParameterResponse();

    //void reset(int activePortCount, int receiverPortCount, int pointCount);

    void resize(int pointCount);

    int pointCount() const
    {
        if (activePortCount() == 0) return 0;
        return _parameterResponses[0]->pointCount();
    }

    int receiverPortCount() const
    {
        if (activePortCount() == 0) return 0;
        return _parameterResponses[0]->receiverPortCount();
    }

    int activePortCount() const
    {
        return _parameterResponses.size();
    }

    Planar::Dsp::ComplexVector64& S(int activePort, int receiverPort)
    {
        return _parameterResponses[activePort]->S(receiverPort);
    }

    ActivePortParameterResponse* activePortResponse(int port)
    {
        return _parameterResponses[port];
    }

private:
    QVector<ActivePortParameterResponse*> _parameterResponses;
};

} //namespace Analyzer
} //namespace VNA
} //namespace Planar

