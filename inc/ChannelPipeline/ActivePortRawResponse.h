#pragma once

#include <QVector>

#include <IndexSegment.h>

#include "ActivePortResponseBase.h"
#include "ReceiverPortResponse.h"

namespace Planar {
namespace VNA {
namespace Analyzer {


class ANALYZERSHARED_EXPORT ActivePortRawResponse : public ActivePortResponseBase
{
public:
    ActivePortRawResponse(int receiverPortCount, int pointCount);

    ~ActivePortRawResponse() override;

    //void reset(int receiverPortCount, int pointCount);

    void resize(int pointCount) override;

    int receiverPortCount() const override
    {
        return _rawResponses.size();
    }

    Planar::Dsp::ComplexVector64& T(int port)
    {
        return _rawResponses[port]->T;
    }

    Planar::Dsp::ComplexVector64& R(int port)
    {
        return _rawResponses[port]->R;
    }

    ReceiverPortResponse* receiverPortResponse(int port)
    {
        return _rawResponses[port];
    }

private:
    QVector<ReceiverPortResponse*> _rawResponses;
};


} //namespace Analyzer
} //namespace VNA
} //namespace Planar
