#pragma once

#include <MeasuringResult.h>

#include "RawChannelStage.h"

namespace Planar {
namespace VNA {
namespace Analyzer {

class ANALYZERSHARED_EXPORT ReceiverCorrectionStage : public StorageRawChannelStage
{
    Q_OBJECT
    ANALYZER_ITEM_UUID(PLANAR_UUID)

public:
    explicit ReceiverCorrectionStage(VnaChannel* channel, int pointCount);
    //~ReceiverCorrectionStage() override;

protected:
    void operation(const ChannelDelta& delta) override;
};


} //namespace Analyzer
} //namespace VNA
} //namespace Planar
