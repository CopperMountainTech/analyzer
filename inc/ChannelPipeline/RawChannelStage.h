#pragma once

#include "ChannelStageBase.h"
#include "ChannelRawResponse.h"
#include "ChannelDelta.h"

namespace Planar {
namespace VNA {
namespace Analyzer {

// base class for inplace stages
class ANALYZERSHARED_EXPORT RawChannelStage : public ChannelStageBase
{
    Q_OBJECT
public:
    explicit RawChannelStage(VnaChannel* channel);
    //~ChannelStageBase() override;

    ChannelRawResponse* input() const
    {
        return _input;
    }

    ChannelRawResponse* output() const
    {
        return _output;
    }

    virtual void attachTo(RawChannelStage* previousStage);

protected:
    ChannelRawResponse* _input = nullptr;
    ChannelRawResponse* _output = nullptr;
};


// base class for not-inplace stages
class ANALYZERSHARED_EXPORT StorageRawChannelStage : public RawChannelStage
{
    Q_OBJECT
public:
    explicit StorageRawChannelStage(VnaChannel* channel, int pointCount);
    ~StorageRawChannelStage() override;

    int pointCount() const
    {
        return _output->pointCount();
    }

    void attachTo(RawChannelStage* previousStage) override;

    // переопределенные методы наследников должны вызывать
    void configureOperation() override;
};


} //namespace Analyzer
} //namespace VNA
} //namespace Planar
