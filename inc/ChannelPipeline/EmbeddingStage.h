#pragma once

#include <MeasuringResult.h>

#include "ParameterChannelStage.h"

namespace Planar {
namespace VNA {
namespace Analyzer {

class ANALYZERSHARED_EXPORT EmbeddingStage : public ParameterChannelStage
{
    Q_OBJECT
    ANALYZER_ITEM_UUID(PLANAR_UUID)

public:
    explicit EmbeddingStage(VnaChannel* channel);
    //~ParameterRatioStage() override;

protected:
    void operation(const ChannelDelta& delta) override;
};


} //namespace Analyzer
} //namespace VNA
} //namespace Planar
