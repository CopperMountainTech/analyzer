#pragma once

#include <QVector>

#include <IndexSegment.h>

#include "AnalyzerCommon.h"

namespace Planar {
namespace VNA {
namespace Analyzer {

class ANALYZERSHARED_EXPORT ActivePortResponseBase
{
public:
    ActivePortResponseBase(int pointCount);
    virtual ~ActivePortResponseBase() {}

    int pointCount() const
    {
        return _receivedPoints.size();
    }

    void clear();

    // переопределенные методы наследников должны вызывать метод базового класса
    virtual void resize(int pointCount);

    virtual int receiverPortCount() const = 0;

    IndexSegment& receivedPoints()
    {
        return _receivedPoints;
    }

    bool isCompleted();
    bool isPointCompleted(int index);

protected:
    IndexSegment _receivedPoints;
};


} //namespace Analyzer
} //namespace VNA
} //namespace Planar
