#pragma once

#include <MeasuringResult.h>

#include "RawChannelStage.h"

namespace Planar {
namespace VNA {
namespace Analyzer {

class ANALYZERSHARED_EXPORT HardwareDataReceiverStage : public StorageRawChannelStage
{
    Q_OBJECT
    ANALYZER_ITEM_UUID(PLANAR_UUID)

public:
    explicit HardwareDataReceiverStage(VnaChannel* channel, int pointCount);
    //~HardwareDateReceiverStage() override;

    //true, если завершено сканирование по одному из активных портов
    bool push(MeasuringResult* result, bool isLastResultInQueue);

protected:
    void operation(const ChannelDelta& delta) override;
};


} //namespace Analyzer
} //namespace VNA
} //namespace Planar
