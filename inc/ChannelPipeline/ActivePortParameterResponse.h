#pragma once

#include <QVector>

#include <IndexSegment.h>
#include <ComplexVector.h>

#include "ActivePortResponseBase.h"

namespace Planar {
namespace VNA {
namespace Analyzer {

class ANALYZERSHARED_EXPORT ActivePortParameterResponse : public ActivePortResponseBase
{
public:
    ActivePortParameterResponse(int receiverPortCount, int pointCount);
    ~ActivePortParameterResponse() override;

    //void reset(int receiverPortCount, int pointCount);

    void resize(int pointCount) override;

    int receiverPortCount() const override
    {
        return _normalizedResponses.size();
    }

    Planar::Dsp::ComplexVector64& S(int port)
    {
        return *(_normalizedResponses[port]);
    }

private:
    QVector<Planar::Dsp::ComplexVector64*> _normalizedResponses;
};


} //namespace Analyzer
} //namespace VNA
} //namespace Planar
