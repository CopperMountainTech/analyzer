#pragma once

#include <QVector>

#include "ActivePortRawResponse.h"

namespace Planar {
namespace VNA {
namespace Analyzer {

class ANALYZERSHARED_EXPORT ChannelRawResponse
{
public:
    ChannelRawResponse(int activePortCount, int receiverPortCount, int pointCount);
    ~ChannelRawResponse();

    //void reset(int activePortCount, int receiverPortCount, int pointCount);

    void resize(int pointCount);

    int pointCount() const
    {
        if (activePortCount() == 0) return 0;
        return _rawResponses[0]->pointCount();
    }

    int receiverPortCount() const
    {
        if (activePortCount() == 0) return 0;
        return _rawResponses[0]->receiverPortCount();
    }

    int activePortCount() const
    {
        return _rawResponses.size();
    }

    Planar::Dsp::ComplexVector64& T(int activePort, int receiverPort)
    {
        return _rawResponses[activePort]->T(receiverPort);
    }

    Planar::Dsp::ComplexVector64& R(int activePort, int receiverPort)
    {
        return _rawResponses[activePort]->R(receiverPort);
    }

    ActivePortRawResponse* activePortResponse(int port)
    {
        return _rawResponses[port];
    }

private:
    QVector<ActivePortRawResponse*> _rawResponses;
};

} //namespace Analyzer
} //namespace VNA
} //namespace Planar

