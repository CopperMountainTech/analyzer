#pragma once

#include <MeasuringResult.h>

#include "RawChannelStage.h"
#include "ParameterChannelStage.h"

namespace Planar {
namespace VNA {
namespace Analyzer {

class ANALYZERSHARED_EXPORT ParameterRatioStage : public StorageParameterChannelStage
{
    Q_OBJECT
    ANALYZER_ITEM_UUID(PLANAR_UUID)

public:
    explicit ParameterRatioStage(VnaChannel* channel, int pointCount);
    //~ParameterRatioStage() override;

    void attachToRaw(RawChannelStage* previousStage);

protected:
    void operation(const ChannelDelta& delta) override;

private:
    ChannelRawResponse* _rawInput = nullptr;
};


} //namespace Analyzer
} //namespace VNA
} //namespace Planar
