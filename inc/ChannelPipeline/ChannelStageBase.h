#pragma once

#include "VnaChannel.h"
#include "ChannelDelta.h"

namespace Planar {
namespace VNA {
namespace Analyzer {

class ANALYZERSHARED_EXPORT ChannelStageBase : public AnalyzerItem
{
    Q_OBJECT
public:
    explicit ChannelStageBase(VnaChannel* channel);
    //~ChannelStageBase() override;

    VnaChannel* channel() const
    {
        return _channel;
    }

signals:
    void outputUpdated(ChannelDelta delta);

public slots:
    void onInputUpdated(ChannelDelta delta);
    void onChannelConfigurationChanged();

protected:
    virtual void operation(const ChannelDelta& delta) = 0;
    virtual void bypassOperation(const ChannelDelta& delta);
    virtual void configureOperation();

    VnaChannel* _channel;
};

} //namespace Analyzer
} //namespace VNA
} //namespace Planar
