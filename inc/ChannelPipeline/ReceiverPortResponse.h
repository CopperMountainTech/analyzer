#pragma once

#include <ComplexVector.h>

#include "AnalyzerCommon.h"

namespace Planar {
namespace VNA {
namespace Analyzer {

class ANALYZERSHARED_EXPORT ReceiverPortResponse
{
public:
    ReceiverPortResponse(int pointCount);

    Planar::Dsp::ComplexVector64 T;
    Planar::Dsp::ComplexVector64 R;
    Planar::Dsp::ComplexVector64 sensitivity;

    int size() const
    {
        return int(T.getSize());
    }

    void resize(int pointCount);
};

} //namespace Analyzer
} //namespace VNA
} //namespace Planar
