#pragma once

#include "ChannelStageBase.h"
#include "ChannelParameterResponse.h"
#include "ChannelDelta.h"

namespace Planar {
namespace VNA {
namespace Analyzer {

// base class for inplace stages
class ANALYZERSHARED_EXPORT ParameterChannelStage : public ChannelStageBase
{
    Q_OBJECT
public:
    explicit ParameterChannelStage(VnaChannel* channel);
    //~ParameterChannelStage() override;

    ChannelParameterResponse* input() const
    {
        return _input;
    }

    ChannelParameterResponse* output() const
    {
        return _output;
    }

    virtual void attachTo(ParameterChannelStage* previousStage);

protected:
    ChannelParameterResponse* _input = nullptr;
    ChannelParameterResponse* _output = nullptr;
};


// base class for not-inplace stages
class ANALYZERSHARED_EXPORT StorageParameterChannelStage : public ParameterChannelStage
{
    Q_OBJECT
public:
    explicit StorageParameterChannelStage(VnaChannel* channel, int pointCount);
    ~StorageParameterChannelStage() override;

    int pointCount() const
    {
        return _output->pointCount();
    }

    void attachTo(ParameterChannelStage* previousStage) override;

    // переопределенные методы наследников должны вызывать
    void configureOperation() override;
};


} //namespace Analyzer
} //namespace VNA
} //namespace Planar
