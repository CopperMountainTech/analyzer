#pragma once

#include <QList>

#include <IndexSegment.h>

#include "AnalyzerCommon.h"

namespace Planar {
namespace VNA {
namespace Analyzer {

struct ChannelPortDelta {
    int portIndex;
    IndexSegment segment;
};


struct ANALYZERSHARED_EXPORT ChannelDelta {
    QList<ChannelPortDelta> portDeltas;

    void clear();

    void append(const ChannelPortDelta& portDelta);
};


} //namespace Analyzer
} //namespace VNA
} //namespace Planar


Q_DECLARE_METATYPE(Planar::VNA::Analyzer::ChannelDelta);

