#ifndef LINEARFREQUENCYSTIMULUS_H
#define LINEARFREQUENCYSTIMULUS_H

#include "FrequencyStimulus.h"
#include "AnalyzerCommon.h"
#include "UUidCommon.h"
#include "Factories.h"

namespace Planar {
namespace VNA {
namespace Analyzer {


class ANALYZERSHARED_EXPORT LinearFrequencyStimulus : public FrequencyStimulus
{
    Q_OBJECT
    ANALYZER_ITEM_UUID(PLANAR_UUID)
    FACTORY_DERIVED_H(StimulusBase, LinearFrequencyStimulus)
    Q_PROPERTY(UnitProperty* powerSlope READ powerSlope NOTIFY stimulusPropertyAdded)

public:
    Q_INVOKABLE LinearFrequencyStimulus(VnaChannel* channel);

    void rebuildRange(VnaScanRange* range) const override;

    UnitProperty* powerSlope();
};

} //namespace Analyzer
} //namespace VNA
} //namespace Planar

#endif // LINEARFREQUENCYSTIMULUS_H
