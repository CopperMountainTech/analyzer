#ifndef STIMULUSBASE_H
#define STIMULUSBASE_H

#define STARTSIZE 201

#include <QString>

#include <Factories.h>
#include <Units.h>
#include <AnalyzerItem.h>
#include <FactoryBase.h>
#include <UuidConstexpr.h>

#include "AnalyzerCommon.h"

namespace Planar {
namespace VNA {
namespace Analyzer {

static constexpr QUuid LinearFrequencyStimulusUuid =
    CreateUuid(0xffb65896, 0x8c3e, 0x4c2a, 0xa171, 0xe372dd7c7d83);

static constexpr QUuid LogarithmFrequencyStimulusUuid =
    CreateUuid(0x123e4567, 0xe89b, 0x12d3, 0xa456, 0x426655440000);

static constexpr QUuid LinearPowerStimulusUuid =
    CreateUuid(0x95810a13, 0xc0db, 0x4d7e, 0xa15b, 0x848518ccdb13);

static constexpr QUuid MultisegmentFrequencyStimulusUuid =
    CreateUuid(0x123e4567, 0xc31b, 0x456e, 0xac3a, 0xb3ae3268a9a6);


class VnaScanRange;
class VnaChannel;

class ANALYZERSHARED_EXPORT StimulusBase : public Planar::AnalyzerItem, public FactoryBase<QUuid>
{
    Q_OBJECT
    ANALYZER_ITEM_UUID(PLANAR_UUID)
    FACTORY_BASE(QUuid, StimulusBase, VnaChannel*, _channel)

public:
    StimulusBase(VnaChannel* channel);
    ~StimulusBase() override {}

    // необходимо вызывать в начале переопределенных методов наследников
    virtual void rebuildRange(VnaScanRange* range) const;

    // число точек сканирования
    // для односегментных стимулов совпадает с pointCount.value()
    virtual int totalPoinCount() const
    {
        return 0;
    }

    // абсцисса первой точки
    virtual Unit startPointX() const
    {
        return Unit();
    }

    // абсцисса последней точки
    virtual Unit stopPointX() const
    {
        return Unit();
    }

    // true, если точки распределены логарифмически в диапазоне сканирования
    bool isLogarithmic() const
    {
        return _isLogarithmic;
    }

signals:
    void propertyChanged();
    void stimulusPropertyAdded();

public slots:
    //void onStimulusPropertyChanged();
    void notifyChanged();

protected:
    // для использования в наследниках в случае когда
    // изменение одного свойства вызывает изменение другого (старт/полоса)
    void blockNotifications();
    void unblockNotifications();

    VnaChannel* _channel;

    bool _notificationRequired;
    int _blockNotificationCounter = 0;

    bool _isLogarithmic = false;
};

} //namespace Analyzer
} //namespace VNA
} //namespace Planar

#endif // STIMULUSBASE_H
