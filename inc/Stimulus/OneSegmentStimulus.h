#pragma once

#include <Properties/IntProperty.h>

#include "StimulusBase.h"
#include "AnalyzerCommon.h"

namespace Planar {
namespace VNA {
namespace Analyzer {

class ANALYZERSHARED_EXPORT OneSegmentStimulus : public StimulusBase
{
    Q_OBJECT
    Q_PROPERTY(IntProperty* pointCount READ pointCount NOTIFY stimulusPropertyAdded)
public:
    OneSegmentStimulus(VnaChannel* channel);

    IntProperty* pointCount() const;

    virtual int totalPoinCount() const override;

public slots:
    void activated() override;
};

} //namespace Analyzer
} //namespace VNA
} //namespace Planar

