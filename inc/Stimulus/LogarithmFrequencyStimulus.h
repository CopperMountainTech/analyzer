#ifndef LOGARITHMFREQUENCYSTIMULUS_H
#define LOGARITHMFREQUENCYSTIMULUS_H

#include "FrequencyStimulus.h"

namespace Planar {
namespace VNA {
namespace Analyzer {


class ANALYZERSHARED_EXPORT LogarithmFrequencyStimulus : public FrequencyStimulus
{
    Q_OBJECT
    ANALYZER_ITEM_UUID(PLANAR_UUID)
    FACTORY_DERIVED_H(StimulusBase, LogarithmFrequencyStimulus)

public:
    Q_INVOKABLE LogarithmFrequencyStimulus(VnaChannel* channel);
    void rebuildRange(VnaScanRange* range) const override;
};

} //namespace Analyzer
} //namespace VNA
} //namespace Planar

#endif // LOGARITHMFREQUENCYSTIMULUS_H
