#ifndef FREQUENCYSTIMULUS_H
#define FREQUENCYSTIMULUS_H

#include "OneSegmentStimulus.h"
#include "Properties/UnitProperty.h"
#include "AnalyzerCommon.h"

namespace Planar {
namespace VNA {
namespace Analyzer {

class ANALYZERSHARED_EXPORT FrequencyStimulus : public OneSegmentStimulus
{
    Q_OBJECT
    Q_PROPERTY(UnitProperty* startFrequency READ startFrequency NOTIFY stimulusPropertyAdded)
    Q_PROPERTY(UnitProperty* stopFrequency READ stopFrequency NOTIFY stimulusPropertyAdded)
    Q_PROPERTY(UnitProperty* centerFrequency READ centerFrequency NOTIFY stimulusPropertyAdded)
    Q_PROPERTY(UnitProperty* bandwidth READ bandwidth NOTIFY stimulusPropertyAdded)

public:
    FrequencyStimulus(VnaChannel* channel);

    UnitProperty* startFrequency() const;
    UnitProperty* stopFrequency() const;
    UnitProperty* centerFrequency() const;
    UnitProperty* bandwidth() const;

    Unit startPointX() const override;
    Unit stopPointX() const override;
};
} //namespace Analyzer
} //namespace VNA
} //namespace Planar

#endif // FREQUENCYSTIMULUS_H
