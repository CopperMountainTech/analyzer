#ifndef LINEARPOWERSTIMULUS_H
#define LINEARPOWERSTIMULUS_H

#include "OneSegmentStimulus.h"
#include "Properties/UnitProperty.h"
#include "AnalyzerCommon.h"

namespace Planar {
namespace VNA {
namespace Analyzer {


class ANALYZERSHARED_EXPORT LinearPowerStimulus : public OneSegmentStimulus
{
    Q_OBJECT
    ANALYZER_ITEM_UUID(PLANAR_UUID)
    FACTORY_DERIVED_H(StimulusBase, LinearPowerStimulus)
    Q_PROPERTY(UnitProperty* startPower READ startPower NOTIFY stimulusPropertyAdded)
    Q_PROPERTY(UnitProperty* stopPower READ stopPower NOTIFY stimulusPropertyAdded)
    Q_PROPERTY(UnitProperty* constantFrequency READ constantFrequency NOTIFY stimulusPropertyAdded)

public:
    Q_INVOKABLE LinearPowerStimulus(VnaChannel* channel);

    void rebuildRange(VnaScanRange* range) const override;

    UnitProperty* startPower() const;
    UnitProperty* stopPower() const;
    UnitProperty* constantFrequency() const;

    Unit startPointX() const override;
    Unit stopPointX() const override;

public slots:
//    void preStartPowerChanging(Planar::Unit& result, bool& isApproved);
//    void onStartPowerChanged(Planar::Unit value);
//    void preStopPowerChanging(Planar::Unit& result, bool& isApproved);
//    void onStopPowerChanged(Planar::Unit value);
//    void preConstFrequencyChanging(Planar::Unit& result, bool& isApproved);
//    void onConstFrequencyChanged(Planar::Unit value);
};

} //namespace Analyzer
} //namespace VNA
} //namespace Planar

#endif // LINEARPOWERSTIMULUS_H
