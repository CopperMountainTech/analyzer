#pragma once

#include <QList>

#include "StimulusBase.h"
#include "VnaScanRange.h"
#include "LinearFrequencyStimulus.h"

namespace Planar {
namespace VNA {
namespace Analyzer {


struct FrequencySegment {
    FrequencySegment();
    FrequencySegment(Unit start, Unit stop, int count, Unit ifbw, Unit power, Unit delaytime);

    Unit startFrequency;
    Unit stopFrequency;
    int  pointCount;
    Unit ifBandwidth;
    Unit outputPower;
    Unit delay;
};


class ANALYZERSHARED_EXPORT MultiSegmentFrequencyStimulus: public StimulusBase
{
    Q_OBJECT
    ANALYZER_ITEM_UUID(PLANAR_UUID)
    FACTORY_DERIVED_H(StimulusBase, MultiSegmentFrequencyStimulus)

public:
    Q_INVOKABLE MultiSegmentFrequencyStimulus(VnaChannel* channel);
    ~MultiSegmentFrequencyStimulus() override;

    virtual int totalPoinCount() const override;

    void rebuildRange(VnaScanRange* range) const override;

    Unit startPointX() const override;
    Unit stopPointX() const override;

    FrequencySegment* at(int i) const;

    void append(FrequencySegment* value);
    void insert(int i, FrequencySegment* value);
    void clear();
    void removeAt(int i);
    void removeFirst();
    void removeLast();

    QList<FrequencySegment*>& segments()
    {
        return _segments;
    }

public slots:
    void activated() override;

private:
    QList<FrequencySegment*> _segments;
};

} //namespace Analyzer
} //namespace VNA
} //namespace Planar

