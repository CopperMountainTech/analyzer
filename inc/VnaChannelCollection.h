#pragma once

#include <AnalyzerItemCollection.h>

#include "VnaChannel.h"

namespace Planar {
namespace VNA {
namespace Analyzer {

class VnaAnalyzer;

class ANALYZERSHARED_EXPORT VnaChannelCollection : public AnalyzerItemCollection<VnaChannel>
{
    Q_OBJECT
    ANALYZER_ITEM_UUID(PLANAR_UUID)
    Q_PROPERTY(VnaChannel* activeChannel READ activeChannel NOTIFY activeItemChanged)

public:
    VnaChannelCollection(VnaAnalyzer* parent);

    Q_INVOKABLE VnaChannel* channel(int index) const;

    VnaChannel* activeChannel() const;

    Q_INVOKABLE void requestAddChannel();
    Q_INVOKABLE void requestAddChannel(int pointCount);

    void addChild(QVariantList params) override;

private:
    VnaAnalyzer* owner();
    //friend void deleteCommandFromQueue(VnaAnalyzer* analyzerObject, const AnalyzerCommand& command);
};

} //namespace Analyzer
} //namespace VNA
} //namespace Planar

Q_DECLARE_METATYPE(Planar::VNA::Analyzer::VnaChannelCollection*)
