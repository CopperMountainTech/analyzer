#pragma once

#include <ScanRangeBase.h>
#include <Abscissa.h>

#include "ChannelPipeline/ActivePortRawResponse.h"

namespace Planar {
namespace VNA {
namespace Analyzer {

class ANALYZERSHARED_EXPORT VnaScanRange : public ScanRangeBase
{
    Q_OBJECT
public:
    VnaScanRange(int stimulusPortCount, int receivingPortCount, int pointCount);
    ~VnaScanRange();

    int pointCount() const
    {
        return _stimulusPorts[0]->pointCount();
    }

    void resize(int pointCount) override;

    int stimulusPortCount()
    {
        return _stimulusPorts.size();
    }

    int recevingPortCount()
    {
        return _stimulusPorts[0]->receiverPortCount();
    }

    Planar::Dsp::ComplexVector64& T(int stimulusPortId, int receivingPortId)
    {
        return _stimulusPorts[stimulusPortId]->T(receivingPortId);
    }

    Planar::Dsp::ComplexVector64& R(int stimulusPortId, int receivingPortId)
    {
        return _stimulusPorts[stimulusPortId]->R(receivingPortId);
    }

    Planar::Dsp::AbstractAbscissa* abscissa() const
    {
        return _abscissa;
    }

    void setAbscissaRnd(const Unit& scaleUnit);

    void setAbscissaSimple();

    void setAbscissaUni(const Unit& min, const Unit& max);

signals:
    void resultReady(MeasuringResult* result);


public slots:
    void onMeasuringResultReady(MeasuringResult* result) override;

private:
    void setAbscissa(Planar::Dsp::AbstractAbscissa* abscissa);

    QVector<ActivePortRawResponse*> _stimulusPorts;
    Planar::Dsp::AbstractAbscissa* _abscissa;
};

} //namespace Analyzer
} //namespace VNA
} //namespace Planar
