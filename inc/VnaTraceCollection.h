#pragma once

#include <AnalyzerItemCollection.h>

#include "VnaTrace.h"

namespace Planar {
namespace VNA {
namespace Analyzer {

class VnaChannel;

class ANALYZERSHARED_EXPORT VnaTraceCollection : public AnalyzerItemCollection<VnaTrace>
{
    Q_OBJECT
    ANALYZER_ITEM_UUID(PLANAR_UUID)
    Q_PROPERTY(VnaTrace* activeTrace READ activeTrace NOTIFY activeItemChanged)

public:
    VnaTraceCollection(VnaChannel* parent);

    Q_INVOKABLE VnaTrace* trace(int index) const;

    VnaTrace* activeTrace() const;

    Q_INVOKABLE void requestAddTrace();
    Q_INVOKABLE void requestAddTrace(MeasurementParameter param, QString traceFormatKey);

    void addChild(QVariantList params) override;

private:
    VnaChannel* owner();
};

} //namespace Analyzer
} //namespace VNA
} //namespace Planar

Q_DECLARE_METATYPE(Planar::VNA::Analyzer::VnaTraceCollection*)
