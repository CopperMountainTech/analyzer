#pragma once
#include "TraceStageBase.h"

#include <Properties/UnitProperty.h>
#include <Properties/BoolProperty.h>

#include "Operations/SmoothingOperation.h"

namespace Planar {
namespace VNA {
namespace Analyzer {

class ANALYZERSHARED_EXPORT SmoothingTraceStage : public TraceStageBase
{
    Q_OBJECT
    ANALYZER_ITEM_UUID(PLANAR_UUID)
    Q_PROPERTY(UnitProperty* aperture READ aperture NOTIFY apertureChanged)
    Q_PROPERTY(BoolProperty* isSmoothing READ isSmoothing NOTIFY smoothingChanged)
public:
    SmoothingTraceStage(VnaTrace* parentItem);
    ~SmoothingTraceStage() override;

    SmoothingOperation* smoothingOperation() const;
    TraceStageOperationBase* operation() const override;

    UnitProperty* aperture() const;
    BoolProperty* isSmoothing() const;
public slots:
    void updateOperation();
signals:
    void apertureChanged();
    void smoothingChanged();
protected:
    UnitProperty* _aperture;
    BoolProperty* _isSmoothing;
};

inline SmoothingOperation* SmoothingTraceStage::smoothingOperation() const
{
    return static_cast<SmoothingOperation*>(_operation);
}

inline TraceStageOperationBase* SmoothingTraceStage::operation() const
{
    return _operation;
}

} // namespace Analyzer
} // namespace VNA
} // namespace Planar
