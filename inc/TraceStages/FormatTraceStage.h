#pragma once
#include "TraceStageBase.h"
#include "Operations/FormatOperationBase.h"

#include <Properties/FactoryProperty.h>

namespace Planar {
namespace VNA {
namespace Analyzer {

class ANALYZERSHARED_EXPORT FormatTraceStage : public TraceStageBase
{
    Q_OBJECT
    ANALYZER_ITEM_UUID(PLANAR_UUID)
    Q_PROPERTY(FormatOperationBase* formatOperationInstance READ formatOperationInstance NOTIFY
               operationChanged)
    Q_PROPERTY(FactoryPropertyBase* format READ format NOTIFY formatChanged)

public:
    FormatTraceStage(VnaTrace* parentItem, QUuid format);
    ~FormatTraceStage() override;

    bool isPolar() const;
    FactoryProperty<QUuid>* format() const;
    FormatOperationBase* formatOperationInstance() const;
    FormatOperationBase* formatOperationFactory() const;
    TraceStageOperationBase* operation() const override;
    IntProperty* scalePosition();
    UnitProperty* scaleReference();
    UnitProperty* scaleDivision();

    void setScale(TraceScaleInfo* scaleInfo);
    void resetScale();

public slots:
    void onScaleReferenceChanged(Planar::Unit scaleRefValue);
    void onScaleDivisionChanged(Planar::Unit scaleDivValue);

private:
    FactoryProperty<QUuid>* _format;

signals:
    void formatChanged();
    void scaleChanged();

protected:
    // позиция сетки графика, начиная от верхней границы
    IntProperty _scalePosition;
    // значение шкалы на позиции _scalePosition
    UnitProperty _scaleReference;
    // цена деления шкалы
    UnitProperty _scaleDivision;
};

inline FormatOperationBase* FormatTraceStage::formatOperationInstance() const
{
    return static_cast<FormatOperationBase*>(_operation)->instance();
}

inline FormatOperationBase* FormatTraceStage::formatOperationFactory() const
{
    return static_cast<FormatOperationBase*>(_operation);
}

inline TraceStageOperationBase* FormatTraceStage::operation() const
{
    return formatOperationInstance();
}

inline IntProperty* FormatTraceStage::scalePosition()
{
    return &_scalePosition;
}

inline UnitProperty* FormatTraceStage::scaleReference()
{
    return &_scaleReference;
}

inline UnitProperty* FormatTraceStage::scaleDivision()
{
    return &_scaleDivision;
}


} // namespace Analyzer
} // namespace VNA
} // namespace Planar
