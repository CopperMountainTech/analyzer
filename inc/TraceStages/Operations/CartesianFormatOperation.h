#pragma once
#include "FormatOperationBase.h"

namespace Planar {
namespace VNA {
namespace Analyzer {

class CartesianFormatOperation : public FormatOperationBase
{
    Q_OBJECT
public:
    CartesianFormatOperation(FormatTraceStage* parentItem);
    ~CartesianFormatOperation() override;

    Dsp::AbstractVector* allocateOutput(int size) override;
    bool isComplexOutput() override;

    void run(Dsp::AbstractVector* source, Dsp::AbstractVector* destination,
             IndexRange dirtyRange) override;
    virtual void runFormat(Dsp::ComplexVector64* source, Dsp::RealVector64* destination,
                           IndexRange dirtyRange) = 0;
};

} // namespace Analyzer
} // namespace VNA
} // namespace Planar
