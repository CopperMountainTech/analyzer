#pragma once
#include "RawOperationBase.h"
#include "ChannelPipeline/ChannelRawResponse.h"


namespace Planar {
namespace VNA {
namespace Analyzer {

class TestRawOperation : public RawOperationBase
{
    Q_OBJECT
    ANALYZER_ITEM_UUID(PLANAR_UUID)
    FACTORY_DERIVED_H(ParameterOperationBase, TestRawOperation)
public:
    Q_INVOKABLE TestRawOperation(MeasurementSelectTraceStage* parentItem);
    ~TestRawOperation() override;

    Dsp::AbstractVector* source() override;
};

} // namespace Analyzer
} // namespace VNA
} // namespace Planar
