#pragma once
#include "TraceStageOperationBase.h"

#include <SmoothTransform.h>

namespace Planar {
namespace VNA {
namespace Analyzer {

class SmoothingTraceStage;

class SmoothingOperation : public TraceStageOperationBase
{
public:
    SmoothingOperation(SmoothingTraceStage* parentItem);
    ~SmoothingOperation() override;

    void run(Dsp::AbstractVector* source, Dsp::AbstractVector* destination,
             IndexRange dirtyRange) override;

    Dsp::AbstractVector* allocateOutput(int size) override;
    bool isComplexOutput() override;

    void resize(int size) override;

    TraceStageBase* stage() override;

private:
    Dsp::ComplexVector64* complex(Dsp::AbstractVector* vector)
    {
        return static_cast<Dsp::ComplexVector64*>(vector);
    }
    Dsp::RealVector64* real(Dsp::AbstractVector* vector)
    {
        return static_cast<Dsp::RealVector64*>(vector);
    }

    SmoothingTraceStage* _stage;

    Dsp::RealVector64 _taps;
    Dsp::SmoothTransform64 _smoother;
    int _smootherHalfSize;
};

} // namespace Analyzer
} // namespace VNA
} // namespace Planar
