#pragma once
#include "RawOperationBase.h"
#include "ChannelPipeline/ChannelRawResponse.h"

namespace Planar {
namespace VNA {
namespace Analyzer {

class ReferenceRawOperation : public RawOperationBase
{
    Q_OBJECT
    ANALYZER_ITEM_UUID(PLANAR_UUID)
    FACTORY_DERIVED_H(ParameterOperationBase, ReferenceRawOperation)
public:
    Q_INVOKABLE ReferenceRawOperation(MeasurementSelectTraceStage* parentItem);
    ~ReferenceRawOperation() override;

    Dsp::AbstractVector* source() override;
};

} // namespace Analyzer
} // namespace VNA
} // namespace Planar
