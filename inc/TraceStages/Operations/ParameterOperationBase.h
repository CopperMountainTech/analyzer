#pragma once
#include "TraceStageOperationBase.h"
#include "VnaParameter.h"

#include <Factories.h>

namespace Planar {
namespace VNA {
namespace Analyzer {

class MeasurementSelectTraceStage;

class ParameterOperationBase : public TraceStageOperationBase
{
    Q_OBJECT
    ANALYZER_ITEM_UUID(PLANAR_UUID)
    FACTORY_BASE(QUuid, ParameterOperationBase, MeasurementSelectTraceStage*, _stage)
public:
    Q_INVOKABLE ParameterOperationBase(MeasurementSelectTraceStage* parentItem);
    ~ParameterOperationBase() override;

    virtual Dsp::AbstractVector* source();
    Dsp::AbstractVector* allocateOutput(int size) override;
    bool isComplexOutput() override;
    void resize(int size) override;

    void run(Dsp::AbstractVector* source, Dsp::AbstractVector* destination,
             IndexRange dirtyRange) override;

    TraceStageBase* stage() override;
    void setVnaParameter(const VnaParameter& vnaParameter);

    VnaParameter vnaParameter() const;

protected:
    virtual int activePortCount();
    virtual int receiverPortCount();
    MeasurementSelectTraceStage* _stage;
    VnaParameter _vnaParameter;
};

} // namespace Analyzer
} // namespace VNA
} // namespace Planar
