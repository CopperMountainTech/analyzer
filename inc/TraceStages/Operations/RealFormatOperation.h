#pragma once
#include "CartesianFormatOperation.h"

namespace Planar {
namespace VNA {
namespace Analyzer {

class RealFormatOperation : public CartesianFormatOperation
{
    Q_OBJECT
    ANALYZER_ITEM_UUID(PLANAR_UUID)
    FACTORY_DERIVED_H(FormatOperationBase, RealFormatOperation)
public:
    Q_INVOKABLE RealFormatOperation(FormatTraceStage* parentItem);
    ~RealFormatOperation() override;

    void runFormat(Dsp::ComplexVector64* source, Dsp::RealVector64* destination,
                   IndexRange dirtyRange) override;
};

} // namespace Analyzer
} // namespace VNA
} // namespace Planar
