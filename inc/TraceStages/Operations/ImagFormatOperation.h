#pragma once
#include "CartesianFormatOperation.h"

namespace Planar {
namespace VNA {
namespace Analyzer {

class ImagFormatOperation : public CartesianFormatOperation
{
    Q_OBJECT
    ANALYZER_ITEM_UUID(PLANAR_UUID)
    FACTORY_DERIVED_H(FormatOperationBase, ImagFormatOperation)
public:
    Q_INVOKABLE ImagFormatOperation(FormatTraceStage* parentItem);
    ~ImagFormatOperation() override;

    void runFormat(Dsp::ComplexVector64* source, Dsp::RealVector64* destination,
                   IndexRange dirtyRange) override;
};

} // namespace Analyzer
} // namespace VNA
} // namespace Planar
