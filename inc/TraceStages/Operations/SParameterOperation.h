#pragma once
#include "ParameterOperationBase.h"
#include "ChannelPipeline/ChannelParameterResponse.h"

namespace Planar {
namespace VNA {
namespace Analyzer {

class SParameterOperation : public ParameterOperationBase
{
    Q_OBJECT
    ANALYZER_ITEM_UUID(PLANAR_UUID)
    FACTORY_DERIVED_H(ParameterOperationBase, SParameterOperation)
public:
    Q_INVOKABLE SParameterOperation(MeasurementSelectTraceStage* parentItem);
    ~SParameterOperation() override;

    Dsp::AbstractVector* source() override;
protected:
    int activePortCount() override;
    int receiverPortCount() override;

    ChannelParameterResponse* _response;
};

} // namespace Analyzer
} // namespace VNA
} // namespace Planar
