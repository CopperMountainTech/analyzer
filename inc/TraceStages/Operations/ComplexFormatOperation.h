#pragma once
#include "FormatOperationBase.h"

namespace Planar {
namespace VNA {
namespace Analyzer {

class ComplexFormatOperation : public FormatOperationBase
{
    Q_OBJECT
    ANALYZER_ITEM_UUID(PLANAR_UUID)
    FACTORY_DERIVED_H(FormatOperationBase, ComplexFormatOperation)
    MULTIKEY_FACTORY_DERIVED_H(PolarCpx)
    MULTIKEY_FACTORY_DERIVED_H(PolarLin)
    MULTIKEY_FACTORY_DERIVED_H(PolarLog)
    MULTIKEY_FACTORY_DERIVED_H(SmithCpx)
    MULTIKEY_FACTORY_DERIVED_H(SmithGjB)
    MULTIKEY_FACTORY_DERIVED_H(SmithLin)
    MULTIKEY_FACTORY_DERIVED_H(SmithLog)
    MULTIKEY_FACTORY_DERIVED_H(SmithRjX)

public:
    Q_INVOKABLE ComplexFormatOperation(FormatTraceStage* parentItem);
    ~ComplexFormatOperation() override;

    Dsp::AbstractVector* allocateOutput(int size) override;
    bool isComplexOutput() override;

    void run(Dsp::AbstractVector* source, Dsp::AbstractVector* destination,
             IndexRange dirtyRange) override;
};

} // namespace Analyzer
} // namespace VNA
} // namespace Planar
