#pragma once
#include <AnalyzerItem.h>

#include <IndexRange.h>
#include <ComplexVector.h>
#include <RealVector.h>


namespace Planar {
namespace VNA {
namespace Analyzer {

class TraceStageBase;

class TraceStageOperationBase : public AnalyzerItem
{
    Q_OBJECT
    ANALYZER_ITEM_UUID(PLANAR_UUID)
public:
    TraceStageOperationBase(TraceStageBase* parentItem);
    ~TraceStageOperationBase() override;

    virtual void run(Planar::Dsp::AbstractVector* source, Planar::Dsp::AbstractVector* destination,
                     Planar::IndexRange dirtyRange) = 0;

    virtual Planar::Dsp::AbstractVector* allocateOutput(int size) = 0;
    virtual bool isComplexOutput() = 0;

    virtual void resize(int size) = 0;

    virtual TraceStageBase* stage() = 0;
};

} // namespace Analyzer
} // namespace VNA
} // namespace Planar
