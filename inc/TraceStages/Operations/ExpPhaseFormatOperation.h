#pragma once
#include "CartesianFormatOperation.h"

namespace Planar {
namespace VNA {
namespace Analyzer {

class ExpPhaseFormatOperation : public CartesianFormatOperation
{
    Q_OBJECT
    ANALYZER_ITEM_UUID(PLANAR_UUID)
    FACTORY_DERIVED_H(FormatOperationBase, ExpPhaseFormatOperation)
public:
    Q_INVOKABLE ExpPhaseFormatOperation(FormatTraceStage* parentItem);
    ~ExpPhaseFormatOperation() override;

    void runFormat(Dsp::ComplexVector64* source, Dsp::RealVector64* destination,
                   IndexRange dirtyRange) override;
    void resize(int size) override;
private:
    Dsp::RealVector64 _deltaR;
};

} // namespace Analyzer
} // namespace VNA
} // namespace Planar
