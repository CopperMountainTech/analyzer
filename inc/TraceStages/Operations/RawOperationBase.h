#pragma once
#include "ParameterOperationBase.h"
#include "ChannelPipeline/ChannelRawResponse.h"

namespace Planar {
namespace VNA {
namespace Analyzer {

class RawOperationBase : public ParameterOperationBase
{
    Q_OBJECT
    ANALYZER_ITEM_UUID(PLANAR_UUID)

public:
    Q_INVOKABLE RawOperationBase(MeasurementSelectTraceStage* parentItem);
    ~RawOperationBase() override;

protected:
    int activePortCount() override;
    int receiverPortCount() override;

    ChannelRawResponse* _response;
};

} // namespace Analyzer
} // namespace VNA
} // namespace Planar

