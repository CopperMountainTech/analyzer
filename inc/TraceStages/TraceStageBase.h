#pragma once
#include "AnalyzerCommon.h"
#include <AnalyzerItem.h>
#include <IndexRange.h>
#include <ComplexVector.h>
#include <RealVector.h>

#include "Operations/TraceStageOperationBase.h"

namespace Planar {
namespace VNA {
namespace Analyzer {

class VnaTrace;

class ANALYZERSHARED_EXPORT TraceStageBase : public AnalyzerItem
{
    Q_OBJECT
    ANALYZER_ITEM_UUID(PLANAR_UUID)
public:
    explicit TraceStageBase(VnaTrace* parentItem);
    ~TraceStageBase() override;

    void connectNextStage(TraceStageBase* nextStage);

    Planar::Dsp::AbstractVector* output() const;
    Planar::IndexRange dirtyRange() const;

    VnaTrace* trace();
    bool isReverseSweep() const;

    TraceStageBase* previousStage() const;

    virtual TraceStageOperationBase* operation() const = 0;

signals:
    void dataReady(Planar::Dsp::AbstractVector* data, Planar::IndexRange dirtyRange);
    void configured(int size);
    void operationChanged();
public slots:
    virtual void process(Planar::Dsp::AbstractVector* data, Planar::IndexRange dirtyRange);
    virtual void configure(int size);
protected:
    Planar::Dsp::ComplexVector64* outputAsComplex() const;
    Planar::Dsp::RealVector64* outputAsReal() const;

    Planar::Dsp::AbstractVector* _output;
    Planar::IndexRange _dirtyRange;
    TraceStageBase* _previousStage;
    VnaTrace* _trace;
    TraceStageOperationBase* _operation;
};

TraceStageBase& operator>>(TraceStageBase& stage1, TraceStageBase& stage2);

inline Dsp::AbstractVector* TraceStageBase::output() const
{
    Q_CHECK_PTR(_output);
    return _output;
}

inline IndexRange TraceStageBase::dirtyRange() const
{
    return _dirtyRange;
}

inline TraceStageBase* TraceStageBase::previousStage() const
{
    Q_CHECK_PTR(_previousStage);
    return _previousStage;
}

inline Dsp::ComplexVector64* TraceStageBase::outputAsComplex() const
{
    Q_CHECK_PTR(_output);
    return static_cast<Dsp::ComplexVector64*>(_output);
}

inline Dsp::RealVector64* TraceStageBase::outputAsReal() const
{
    Q_CHECK_PTR(_output);
    return static_cast<Dsp::RealVector64*>(_output);
}


} // namespace Analyzer
} // namespace VNA
} // namespace Planar
