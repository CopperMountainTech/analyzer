#pragma once
#include "TraceStageBase.h"
#include "AnalyzerCommon.h"
#include "VnaScanRange.h"
#include "Operations/ParameterOperationBase.h"
#include "VnaParameter.h"
#include "ChannelPipeline/ChannelDelta.h"

#include <Properties/EnumProperty.h>

namespace Planar {
namespace VNA {
namespace Analyzer {


class ANALYZERSHARED_EXPORT MeasurementSelectTraceStage : public TraceStageBase
{
    Q_OBJECT
    ANALYZER_ITEM_UUID(PLANAR_UUID)
    Q_PROPERTY(ParameterOperationBase* parameterOperationInstance READ parameterOperationInstance NOTIFY
               operationChanged)
    Q_PROPERTY(EnumPropertyBase* parameter READ parameter NOTIFY parameterChanged)
public:
    MeasurementSelectTraceStage(VnaTrace* parentItem, MeasurementParameter param);
    ~MeasurementSelectTraceStage() override;

    EnumProperty<MeasurementParameter>* parameter() const;

    ParameterOperationBase* parameterOperationInstance() const;
    ParameterOperationBase* parameterOperationFactory() const;
    TraceStageOperationBase* operation() const override;

    VnaParameter vnaParameter() const;
public slots:
    void processRawData(ChannelDelta delta);
    void processParameterData(ChannelDelta delta);
    void onParameterChanged(int newValue);
signals:
    void parameterChanged();
private:
    VnaParameter measurementParameterToVnaParameter(MeasurementParameter parameter);

    EnumProperty<MeasurementParameter>* _parameter;
    int _portIndex;
};

inline EnumProperty<MeasurementParameter>* MeasurementSelectTraceStage::parameter() const
{
    return _parameter;
}

inline ParameterOperationBase* MeasurementSelectTraceStage::parameterOperationInstance() const
{
    return static_cast<ParameterOperationBase*>(_operation)->instance();
}

inline ParameterOperationBase* MeasurementSelectTraceStage::parameterOperationFactory() const
{
    return static_cast<ParameterOperationBase*>(_operation);
}

inline TraceStageOperationBase* MeasurementSelectTraceStage::operation() const
{
    return parameterOperationInstance();
}

} // namespace Analyzer
} // namespace VNA
} // namespace Planar
