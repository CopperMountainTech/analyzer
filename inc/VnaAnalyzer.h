#ifndef ANALYZER_H
#define ANALYZER_H

#include <QThread>
#include <QThreadPool>
#include <QtConcurrent/qtconcurrentrun.h>
#include <QQueue>
#include <QFuture>

#include <AnalyzerItem.h>
#include <RootBaseItem.h>
#include <DeviceManager.h>  // Диспетчер устройств
#include <VnaHw.h>          // Интерфейс IVNA

#include "AnalyzerCommon.h"
#include "VnaChannelCollection.h"



namespace Planar {
namespace VNA {
namespace Analyzer {

class ANALYZERSHARED_EXPORT VnaAnalyzer : public AnalyzerItem
{
    Q_OBJECT
    ANALYZER_ITEM_UUID(PLANAR_UUID)
    Q_PROPERTY(VnaChannelCollection* channels READ channels NOTIFY analyzerPropertyAdded)
    //Q_PROPERTY(Planar::VNA::Hardware::IVna* vna READ vna NOTIFY vnaChanged)
    Q_PROPERTY(AnalyzerItem* vna READ vna NOTIFY vnaChanged)
    Q_PROPERTY(int activePortCount READ activePortCount NOTIFY vnaChanged)
    Q_PROPERTY(int receiverPortCount READ receiverPortCount NOTIFY vnaChanged)

public:
    ~VnaAnalyzer() override;

    VnaChannelCollection* channels()
    {
        return  &_channelCollection;
    }

    Q_INVOKABLE VnaChannel* activeChannel()   // Q_INVOKABLE для единообразия))
    {
        return _channelCollection.activeItem();
    }

    Q_INVOKABLE VnaTrace* activeTrace();

    Q_INVOKABLE VnaMarker* activeMarker();


    Planar::VNA::Hardware::IVna* vna()
    {
        return _vna;
    }

    QQueue<AnalyzerCommand>* commandQueue() override
    {
        return &_commandQueue;
    }

//    Planar::Hardware::DeviceManager* deviceManager()
//    {
//        return _deviceManager;
//    }

    bool isInitialized();
    void initialize(Planar::VNA::Hardware::IVna* vna);

    //void deInitialize();

    static VnaAnalyzer* instance();
    static VnaAnalyzer* createInstance(RootBaseItem* root);
    static bool isInstanceInitialized();

    // constraints

    static int activePortCount();
    static int receiverPortCount();

    static int minPointCount();
    static int maxPointCount();
    static int defaultPointCount();

    static Unit minSynthFrequency();
    static Unit maxSynthFrequency();
    static Unit defaultSynthFrequency();

    static Unit maxPowerSlope();

    static Unit minSynthPower();
    static Unit maxSynthPower();
    static Unit defaultSynthPower();

    static Unit minIfBandwidth();
    static Unit maxIfBandwidth();
    static Unit defaultIfBandwidth();

    Q_INVOKABLE void requestInitializeNewDevice(AnalyzerItem* vna);

signals:
    void commandAppended();
    void activeChannelChanged(int index);
    void analyzerPropertyAdded();
    void vnaChanged();

public slots:
    void analyzerReady();

protected slots:
    void onBeforeChannelAdded();
    void onChannelAdded(AnalyzerItem* item);
    void onBeforeChannelRemoved(AnalyzerItem* item);
    void onChannelRemoved();
    void onInitializeNewDevice(QVariant value);

protected:
    VnaAnalyzer(AnalyzerItem* parent);

    //void removeChannel(int index);
    //void removeActiveChannel();

    //void removeChild(int index) override;

    //void addDefaultChild() override;

    // Метод добавления команды в очередь команд
    void pushCommand(const AnalyzerCommand& command,
                     PushingType pushType = PushingType::defaultType) override;
    void addCommandToQueue(const AnalyzerCommand& command,
                           PushingType pushType = PushingType::defaultType) override;
    bool _started;

    StackUndo _stack;

private:
    inline void startChannelsStopWaiting();
    void waitingChannelsStop();

    //void rebindChannels();
    void bindChannel(VnaChannel* channel);
    void unbindChannel(VnaChannel* channel);
    //void startChannel(VnaChannel* channel);
    //void stopChannel(VnaChannel* channel);

    VnaChannelCollection _channelCollection;

    //очередь команд
    QQueue<AnalyzerCommand> _commandQueue;

    QThreadPool* _analyzerThreadPool;
    QFuture<void> _future;
    QFutureWatcher<void> _channelsStopWatcher;

    static VnaAnalyzer* _instance;
//    Planar::Hardware::DeviceManager* _deviceManager;
    Planar::VNA::Hardware::IVna* _vna;

    CommandProperty _initializeNewDevice;

    friend void deleteCommandFromQueue(AnalyzerItem* analyzerObject, const AnalyzerCommand& command);
};

} //namespace Analyzer
} //namespace VNA
} //namespace Planar

#endif // ANALYZER_H
