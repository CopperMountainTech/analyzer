#pragma once

#include <AnalyzerItem.h>
#include <Properties/UnitProperty.h>

#include "AnalyzerCommon.h"

namespace Planar {
namespace VNA {
namespace Analyzer {

class ANALYZERSHARED_EXPORT FrequencyBandSelector : public QObject
{
    Q_OBJECT
public:
    FrequencyBandSelector(AnalyzerItem* parent);

    UnitProperty* start()
    {
        return &_start;
    }

    UnitProperty* stop()
    {
        return &_stop;
    }

    UnitProperty* center()
    {
        return &_center;
    }

    UnitProperty* bandwidth()
    {
        return &_bandwidth;
    }

signals:
    void bandChanged();

private slots:
    void onStartStopChanged();
    void onCenterBandwidthChanged();

private:
    bool _isStartStopEnabled = true;
    bool _isCenterBandwidthEnabled = true;

    UnitProperty _start;
    UnitProperty _stop;
    UnitProperty _center;
    UnitProperty _bandwidth;
};

} //namespace Analyzer
} //namespace VNA
} //namespace Planar
