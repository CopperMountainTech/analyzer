#pragma once

#include <QtCore/qglobal.h>
#include <QtCore/qobjectdefs.h>

#include <FoundationGlobal.h>
#include <UUidCommon.h>
#include <Units.h>

#if defined(ANALYZER_LIBRARY)
#  define ANALYZERSHARED_EXPORT Q_DECL_EXPORT
#else
#  define ANALYZERSHARED_EXPORT Q_DECL_IMPORT
#endif

//enum class PropertyID : int {
//    ChannelPointCount,
//    ChannelAmplitude,
//    ChannelStart,
//    ChannelStop,
//    ChannelNoise
//};

////TODO: Определить список команд
//enum class CommandID : int {
//    SetFreqStart,
//    SetFreqStop,
//    SetFreqType,
//    SetBandwidth,
//    SetAmplitude,
//    AddTrace,
//    AddChannel,
//    DeleteTrace,
//    DeleteChannel
//};


namespace Planar {
namespace VNA {
//ANALYZERSHARED_EXPORT Q_NAMESPACE
namespace Analyzer {
ANALYZERSHARED_EXPORT Q_NAMESPACE


//-----------------------TODO from hardware ----------


enum class MeasurementParameter : int {
    S11,
    S12,
    S21,
    S22,
    AbsT11,
    AbsT12,
    AbsT21,
    AbsT22,
    AbsR11,
    AbsR12,
    AbsR21,
    AbsR22,
};
Q_ENUM_NS(MeasurementParameter);


enum class TraceFormat : int {
    LogMag,
    Phase,
    ExtPhase,
    GroupDelay,
    SWR,
    Real,
    Imag,
    LinMag,
    SmithLin,
    SmithLog,
    SmithCpx,
    SmithRjX,
    SmithGjB,
    PolarLin,
    PolarLog,
    PolarCpx
};
Q_ENUM_NS(TraceFormat);

enum class ScanMode { Linear, Logarithmic };
Q_ENUM_NS(ScanMode);

} //namespace Analyzer
} //namespace VNA
} //namespace Planar
