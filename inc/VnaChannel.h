#ifndef VNACHANNEL_H
#define VNACHANNEL_H

#include <QThreadPool>
#include <QQueue>
#include <QFuture>
#include <QFutureWatcher>
#include <QTimer>
#include <QMutex>

#include <ComplexVector.h>
#include <MultiportVector.h>
#include <AnalyzerItem.h>
#include <VnaHw.h>

#include <Properties/IntProperty.h>
#include <Properties/UnitProperty.h>
#include <Properties/EnumProperty.h>
#include <Properties/BoolProperty.h>
#include <Properties/DoubleProperty.h>
#include <Properties/FabricProperty.h>
#include <Properties/FactoryProperty.h>

#include "ChannelPipeline/ChannelDelta.h"
#include "TraceResult.h"
#include "VnaTraceCollection.h"
#include "Stimulus/StimulusBase.h"
//#include "Stimulus/FrequencyAdapter.h"
#include "VnaScanRange.h"
#include "FrequencyRangeSelector.h"



namespace Planar {
namespace VNA {
//namespace Hardware {
//class IBinder;
//}
namespace Analyzer {

class VnaAnalyzer;
class HardwareDataReceiverStage;
class ParameterRatioStage;
class EmbeddingStage;
class ReceiverCorrectionStage;
class ChannelParameterResponse;
class ChannelRawResponse;

extern ANALYZERSHARED_EXPORT Planar::BiMap<QUuid, QString> stimulusTypeBiMap2;

//Q_DECLARE_METATYPE(TraceResult*)

struct DataQueue {
    DataQueue() : index(0) {}

    void deQueue()
    {
        if (!queue.empty()) {
            delete queue.head();
            queue.dequeue();
            index--;
        } else {
            qDebug() << "delete queue.head()";
        }
    }

    void pushBack(MeasuringResult* result)
    {
        queue.push_back(result);
//        qDebug() << queue.size();
//        //защита от переполнения. Стирает старые данные, если очередь переполнена
//        if (!isFull()) {
//            queue.push_back(object);
//            index++;
//        } else {
//            deQueue();
//            pushBack(object);
//        }
    }

    bool isFull()
    {
        return index >= maxIndex;
    }

    static const int maxIndex = 5000;
    QQueue<MeasuringResult*> queue;
    int index;
};


//------------------------------- VnaChannel --------------------------------------------------


class ANALYZERSHARED_EXPORT VnaChannel : public AnalyzerItem
{
    Q_OBJECT
    ANALYZER_ITEM_UUID(PLANAR_UUID)
    Q_PROPERTY(StimulusBase* stimulusInstance READ stimulus NOTIFY stimulusInstanceChanged)
    Q_PROPERTY(IntProperty* pointCount READ pointCount NOTIFY channelPropertyAdded)
    Q_PROPERTY(UnitProperty* startFrequency READ startFrequency NOTIFY channelPropertyAdded)
    Q_PROPERTY(UnitProperty* stopFrequency READ stopFrequency NOTIFY channelPropertyAdded)
    Q_PROPERTY(BoolProperty* isRunning READ isRunning NOTIFY channelPropertyAdded)
    Q_PROPERTY(IntProperty* scaleDivisionCount READ scaleDivisionCount NOTIFY channelPropertyAdded)
    Q_PROPERTY(FactoryPropertyBase* stimulusName READ stimulusName NOTIFY stimulusNameChanged)
    Q_PROPERTY(VnaTraceCollection* traces READ traces NOTIFY channelPropertyAdded)
    Q_PROPERTY(AnalyzerItem* binder READ hwBinder NOTIFY binderChanged)
    Q_PROPERTY(UnitProperty* ifbw READ ifBandwidth NOTIFY channelPropertyAdded)
    Q_PROPERTY(BoolProperty* isReverseScan READ isReverseScan NOTIFY channelPropertyAdded)

public:
    VnaChannel(VnaAnalyzer* analyzer, int startPointCount = 201);

    ~VnaChannel() override;

    VnaTraceCollection* traces()
    {
        return  &_traceCollection;
    }

    Q_INVOKABLE VnaTrace* activeTrace()     // Q_INVOKABLE для единообразия))
    {
        return _traceCollection.activeItem();
    }

    Q_INVOKABLE VnaMarker* activeMarker();

    FrequencyBandSelector* frequencyBand()
    {
        return &_frequencyBand;
    }

    IntProperty* pointCount();
    UnitProperty* ifBandwidth();

    UnitProperty* startFrequency();
    UnitProperty* stopFrequency();
    UnitProperty* centerFrequency();
    UnitProperty* bandwidth();
    UnitProperty* constantPower();
    UnitProperty* powerSlope();

    UnitProperty* startPower();
    UnitProperty* stopPower();
    UnitProperty* constantFrequency();

    // TODO - убрать ASAP
    EnumProperty<ScanMode>* scanMode();

    BoolProperty* isRunning();
    BoolProperty* isReverseScan();
    IntProperty* scaleDivisionCount();
    FactoryProperty<QUuid>* stimulusName();

    void workProcedure();
    bool isStopped() const;
    void requireBreak();

    StimulusBase* stimulus()
    {
        return _stimulus->instance();
    }

    StimulusBase* stimulusFactory()
    {
        return _stimulus;
    }

    VnaScanRange& scanRange();

    Hardware::IBinder* hwBinder()
    {
        return _hwBinder;
    }
    void setHwBinder(Hardware::IBinder* binder);

//    Unit minFrequencyHw();
//    Unit maxFrequencyHw();
//    Unit minPowerHw();
//    Unit maxPowerHw();

    ChannelParameterResponse* parameterPipelineOutput();
    ChannelRawResponse*       rawPipelineOutput();

signals:
    //void dataChanged();

    // TODO: obsolete, check & remove
    //void rangeRebuilded();
    void rangeUpdated();
    void commandAppended();

    void parameterPipelineOutputUpdated(ChannelDelta delta);
    void rawPipelineOutputUpdated(ChannelDelta delta);

    void channelPropertyAdded();
    void stimulusNameChanged();

    // сменился инстанс стимулуса
    void stimulusInstanceChanged();

    // сменился инстанс стимулуса или одно из свойств
    void stimulusChanged();

    // для реконфигурации стейждей
    // выполнен ребилд scanRange и очищены очереди
    void configurationChanged();

    void binderChanged();

public slots:
    void channelReady();
    void onMeasuringResultReady(MeasuringResult* result);

    void onIsRunningChanged(bool value);

    void onStimulusChanged();

    AnalyzerWriteStream* streamWriter() override
    {
        return &_streamWriterChannel;
    }

    QQueue<AnalyzerCommand>* commandQueue() override
    {
        return &_commandQueue;
    }

protected:
    // Метод добавления команды в очередь команд
    void pushCommand(const AnalyzerCommand& command,
                     PushingType pushType = PushingType::defaultType) override;
    void addCommandToQueue(const AnalyzerCommand& command,
                           PushingType pushType = PushingType::defaultType) override;

private:
    void rebuildRange();
    void updateRange();

    void clearDataQueues();

    // true, если достигнут конец диапазона
    bool resultToRange(MeasuringResult* result);

    void startWorkFlow();
    void moveDataToLocal();

    VnaTraceCollection _traceCollection;

    QThreadPool* _channelThreadPool;
    //QQueue<TraceResult*> _result;
    StimulusBase* _stimulus;

    IntProperty _pointCount;

    FrequencyBandSelector _frequencyBand;

    UnitProperty _constantPower;
    UnitProperty _constantFrequency;
    UnitProperty _ifBandwidth;

    UnitProperty _startPower;
    UnitProperty _stopPower;
    UnitProperty _powerSlope;

    EnumProperty<ScanMode> _scanMode;
    FactoryProperty<QUuid>* _stimulusName;

    BoolProperty _isRunning;
    IntProperty _scaleDivisionCount;

    BoolProperty _isReverseScan;

    QFuture<void> _future;
    QFutureWatcher<void> _workWatcher;

    //очередь команд
    QQueue<AnalyzerCommand> _commandQueue;

    VnaScanRange _scanRange;

    VnaAnalyzer* _analyzer;

    bool _dirtyEmulation;

    //запрос завершения workProcedure ASAP при true
    bool _breakRequired = false;

    AnalyzerWriteStream _streamWriterChannel;

    Hardware::IBinder* _hwBinder;
    //очередь данных
    DataQueue _dataQueue;
    DataQueue _localDataQueue;

    QMutex _queueMutex;

    bool _justRebuilded = false;

    HardwareDataReceiverStage*  _receiverStage;
    ReceiverCorrectionStage*    _receiverCorrectionStage;
    ParameterRatioStage*        _ratioStage;
    EmbeddingStage*             _embeddingStage;
};


//inline Planar::MultiportVector<Planar::Dsp::Complex64>& VnaChannel::oldScanRange()
//{
//    return _oldRange;
//}

inline VnaScanRange& VnaChannel::scanRange()
{
    return _scanRange;
}

inline FactoryProperty<QUuid>* VnaChannel::stimulusName()
{
    return _stimulusName;
}

inline IntProperty* VnaChannel::pointCount()
{
    return &_pointCount;
}

inline UnitProperty* VnaChannel::startFrequency()
{
    return _frequencyBand.start();
}

inline UnitProperty* VnaChannel::stopFrequency()
{
    return _frequencyBand.stop();
}

inline UnitProperty* VnaChannel::centerFrequency()
{
    return _frequencyBand.center();
}

inline UnitProperty* VnaChannel::bandwidth()
{
    return _frequencyBand.bandwidth();
}

inline UnitProperty* VnaChannel::constantPower()
{
    return &_constantPower;
}

inline UnitProperty* VnaChannel::powerSlope()
{
    return &_powerSlope;
}

inline UnitProperty* VnaChannel::constantFrequency()
{
    return &_constantFrequency;
}

inline UnitProperty* VnaChannel::ifBandwidth()
{
    return &_ifBandwidth;
}

inline UnitProperty* VnaChannel::startPower()
{
    return &_startPower;
}

inline UnitProperty* VnaChannel::stopPower()
{
    return &_stopPower;
}


inline EnumProperty<ScanMode>* VnaChannel::scanMode()
{
    return &_scanMode;
}

inline BoolProperty* VnaChannel::isRunning()
{
    return &_isRunning;
}

inline BoolProperty* VnaChannel::isReverseScan()
{
    return &_isReverseScan;
}

inline IntProperty* VnaChannel::scaleDivisionCount()
{
    return &_scaleDivisionCount;
}

} //namespace Analyzer
} //namespace VNA
} //namespace Planar

//Q_DECLARE_METATYPE(Planar::VNA::Analyzer::VnaChannel*)

#endif // VNACHANNEL_H
